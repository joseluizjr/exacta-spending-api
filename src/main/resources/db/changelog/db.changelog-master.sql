--liquibase formatted sql

--changeset José Luiz Junior:20200805170000
--comment: Create produto spents
CREATE TABLE spents
(
    id BIGINT NOT NULL PRIMARY KEY,
    owner VARCHAR(150) NOT NULL,
    description VARCHAR(150) NOT NULL,
    date_time TIMESTAMP NOT NULL,
    price DECIMAL(15,2) NOT NULL
);

CREATE SEQUENCE spents_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;

--rollback DROP TABLE spents; DROP SEQUENCE spents_id_seq;

--changeset José Luiz Junior:20200810180000
--comment: add tags column in spents table

ALTER TABLE spents ADD COLUMN tags TEXT not null;

--rollback ALTER TABLE spents DROP COLUMN tags;