package br.com.exactaworks.exactaspending.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Entity
@Table(name = "spents")
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Spent {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "spents_id_seq")
    @SequenceGenerator(name = "spents_id_seq", sequenceName = "spents_id_seq", allocationSize = 1)
    @EqualsAndHashCode.Include
    private Long id;
    private String owner;
    private String description;
    @Column(name = "date_time")
    private OffsetDateTime dateTime;
    private BigDecimal price;
    private String tags;

}
