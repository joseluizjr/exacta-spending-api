package br.com.exactaworks.exactaspending.domain.repository;

import br.com.exactaworks.exactaspending.domain.model.Spent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpentRepository extends JpaRepository<Spent, Long> {
}
