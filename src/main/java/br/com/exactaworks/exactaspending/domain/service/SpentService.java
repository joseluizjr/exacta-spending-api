package br.com.exactaworks.exactaspending.domain.service;

import br.com.exactaworks.exactaspending.domain.exception.SpentNotFoundException;
import br.com.exactaworks.exactaspending.domain.model.Spent;
import br.com.exactaworks.exactaspending.domain.repository.SpentRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SpentService {

    private final SpentRepository spentRepository;

    public Spent save(Spent spent){
        return spentRepository.save(spent);
    }

    public Spent findById(Long id){
        return spentRepository.findById(id)
                .orElseThrow(() -> new SpentNotFoundException(id));
    }

    public Page<Spent> findAll(Pageable pageable){
        return spentRepository.findAll(pageable);
    }

}
