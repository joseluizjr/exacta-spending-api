package br.com.exactaworks.exactaspending.domain.exception;

public class SpentNotFoundException extends EntityNotFoundException{

    public SpentNotFoundException(String message) {
        super(message);
    }

    public SpentNotFoundException(Long spentId){
        this(String.format("Não existe gasto cadastrado com código %d", spentId));
    }

}
