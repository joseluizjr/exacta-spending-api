package br.com.exactaworks.exactaspending.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@NoArgsConstructor
@Getter @Setter
public class SpentDetailModel {

    private Long id;
    private String owner;
    private String description;
    private OffsetDateTime dateTime;
    private BigDecimal price;
    private String tags;

}
