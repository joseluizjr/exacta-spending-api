package br.com.exactaworks.exactaspending.api.controller;

import javax.validation.Valid;

import br.com.exactaworks.exactaspending.api.assembler.SpentDetailModelAssembler;
import br.com.exactaworks.exactaspending.api.assembler.SpentListModelAssembler;
import br.com.exactaworks.exactaspending.api.disassembler.NewSpentModelDisassembler;
import br.com.exactaworks.exactaspending.api.model.NewSpentModel;
import br.com.exactaworks.exactaspending.api.model.SpentDetailModel;
import br.com.exactaworks.exactaspending.api.model.SpentListModel;
import br.com.exactaworks.exactaspending.domain.model.Spent;
import br.com.exactaworks.exactaspending.domain.service.SpentService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/spents")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SpentController {

    private final SpentService spentService;
    private final NewSpentModelDisassembler newSpentModelDisassembler;
    private final SpentDetailModelAssembler spentDetailModelAssembler;
    private final SpentListModelAssembler spentListModelAssembler;

    @GetMapping
    public Page<SpentListModel> findAll(Pageable pageable){
        Page<Spent> spentsPage = spentService.findAll(pageable);
        return spentListModelAssembler.toModel(spentsPage);
    }

    @GetMapping("{spentId}")
    public SpentDetailModel findById(@PathVariable Long spentId){
        Spent spent = spentService.findById(spentId);
        return spentDetailModelAssembler.toModel(spent);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SpentDetailModel save(@RequestBody @Valid NewSpentModel model){
        Spent savedSpent = spentService.save(newSpentModelDisassembler.toModel(model));
        return spentDetailModelAssembler.toModel(savedSpent);
    }

}
