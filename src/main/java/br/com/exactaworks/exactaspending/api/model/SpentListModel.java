package br.com.exactaworks.exactaspending.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@NoArgsConstructor
@Getter
@Setter
public class SpentListModel {

    private Long Id;
    private String owner;
    private String description;
    private BigDecimal price;
    private OffsetDateTime dateTime;

}
