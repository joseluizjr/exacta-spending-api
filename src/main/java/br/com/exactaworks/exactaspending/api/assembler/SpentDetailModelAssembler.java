package br.com.exactaworks.exactaspending.api.assembler;

import br.com.exactaworks.exactaspending.api.model.SpentDetailModel;
import br.com.exactaworks.exactaspending.domain.model.Spent;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SpentDetailModelAssembler {

    SpentDetailModel toModel(Spent model);

}
