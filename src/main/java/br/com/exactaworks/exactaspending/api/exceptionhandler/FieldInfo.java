package br.com.exactaworks.exactaspending.api.exceptionhandler;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class FieldInfo {

    private String name;
    private String userMessage;

}
