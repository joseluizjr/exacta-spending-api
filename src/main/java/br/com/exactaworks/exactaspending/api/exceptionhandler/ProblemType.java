package br.com.exactaworks.exactaspending.api.exceptionhandler;

import lombok.Getter;

@Getter
public enum ProblemType {

    RESOURCE_NOT_FOUND("/resource-not-found", "Recurso não encontrado"),
    BUSINESS_ERROR("/business-error", "Violação de regra de negócio"),
    INVALID_PROPERTY("/invalid-property", "Propriedade inválida"),
    INCOMPREHENSIBLE_MESSAGE("/incomprehensible-message", "Mensagem incompreensível"),
    INVALID_PARAMETER("/invalid-parameter", "Parâmetro inválido"),
    INVALID_DATA("/invalid-data", "Dados inválidos"),
    SYSTEM_ERROR("/system-error", "Erro de sistema");

    private final String title;
    private final String uri;

    ProblemType(String path, String title){
        this.uri = String.format("https://exactaworks.com.br%s", path);
        this.title = title;
    }

}
