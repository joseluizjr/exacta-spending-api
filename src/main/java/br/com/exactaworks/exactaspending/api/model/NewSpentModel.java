package br.com.exactaworks.exactaspending.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@NoArgsConstructor
@Getter @Setter
public class NewSpentModel {

    @NotNull
    @NotBlank
    @Size(min = 2, max = 150)
    private String owner;
    @NotNull
    @NotBlank
    @Size(min = 2, max = 150)
    private String description;
    @NotNull
    private OffsetDateTime dateTime;
    @NotNull
    @DecimalMin(value = "0.00", inclusive = false)
    @DecimalMax(value = "9999999999999.99")
    @Digits(integer = 13, fraction = 2)
    private BigDecimal price;
    @NotNull
    @NotBlank
    private String tags;

}
