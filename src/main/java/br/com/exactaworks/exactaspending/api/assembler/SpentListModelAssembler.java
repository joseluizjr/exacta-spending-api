package br.com.exactaworks.exactaspending.api.assembler;

import br.com.exactaworks.exactaspending.api.model.NewSpentModel;
import br.com.exactaworks.exactaspending.api.model.SpentListModel;
import br.com.exactaworks.exactaspending.domain.model.Spent;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface SpentListModelAssembler {

    abstract SpentListModel toModel(Spent model);

    default Page<SpentListModel> toModel(Page<Spent> model){
        return model.map(this::toModel);
    }

}
