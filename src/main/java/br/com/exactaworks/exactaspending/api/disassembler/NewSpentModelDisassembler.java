package br.com.exactaworks.exactaspending.api.disassembler;

import br.com.exactaworks.exactaspending.api.model.NewSpentModel;
import br.com.exactaworks.exactaspending.domain.model.Spent;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface NewSpentModelDisassembler {

    @Mapping(target = "id", ignore = true)
    Spent toModel(NewSpentModel model);

}
