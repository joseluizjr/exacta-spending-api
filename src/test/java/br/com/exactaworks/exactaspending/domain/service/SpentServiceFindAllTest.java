package br.com.exactaworks.exactaspending.domain.service;

import br.com.exactaworks.exactaspending.domain.model.Spent;
import br.com.exactaworks.exactaspending.domain.repository.SpentRepository;
import br.com.exactaworks.exactaspending.test.util.modebuilder.SpentModelBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class SpentServiceFindAllTest {

    @Autowired
    private SpentRepository spentRepository;

    private SpentService spentService;

    private final SpentModelBuilder spentModelBuilder = new SpentModelBuilder();

    private int spentsToGenerate = 100;

    @BeforeEach
    public void setup(){
        List<Spent> spents = spentModelBuilder.generateMany(spentsToGenerate);
        spentRepository.saveAll(spents);
        spentService = new SpentService(spentRepository);
    }

    @Test
    public void getWithSpecificSize(){
        int size = 20;
        int page = 0;
        Page<Spent> spents = spentService.findAll(PageRequest.of(page, size));
        assertThat(spents).isNotNull();
        assertThat(spents.getNumberOfElements()).isEqualTo(size);
        assertThat(spents.getPageable().getPageNumber()).isEqualTo(page);
        assertThat(spents.getTotalPages()).isEqualTo(spentsToGenerate / size);
        assertThat(spents.getTotalElements()).isEqualTo(spentsToGenerate);
    }

    @AfterEach
    public void clearDB(){
        spentRepository.deleteAll();
    }

}
