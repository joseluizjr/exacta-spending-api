package br.com.exactaworks.exactaspending.domain.service;

import br.com.exactaworks.exactaspending.domain.exception.SpentNotFoundException;
import br.com.exactaworks.exactaspending.domain.model.Spent;
import br.com.exactaworks.exactaspending.domain.repository.SpentRepository;
import br.com.exactaworks.exactaspending.test.util.modebuilder.SpentModelBuilder;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class SpentServiceFindByIdTest {

    @Autowired
    private SpentRepository spentRepository;

    private SpentService spentService;

    private final SpentModelBuilder spentModelBuilder = new SpentModelBuilder();

    @BeforeEach
    public void setup(){
        spentService = new SpentService(spentRepository);
    }

    @AfterEach
    public void clearDB(){
        spentRepository.deleteAll();
    }

    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    @Nested
    class HasSpentWithId{

        private Spent spent;
        private Spent savedSpent;

        @BeforeAll
        public void setup(){
            spent = spentModelBuilder.generate();
            spentRepository.save(spent);
            savedSpent = spentService.findById(spent.getId());
        }

        @Test
        public void checkIsNotNull(){
            assertThat(savedSpent).isNotNull();
        }

        @Test
        public void checkSpentInformations(){
            assertThat(savedSpent.getId()).isNotNull();
            assertThat(savedSpent.getDateTime()).isEqualTo(spent.getDateTime());
            assertThat(savedSpent.getDescription()).isEqualTo(spent.getDescription());
            assertThat(savedSpent.getOwner()).isEqualTo(spent.getOwner());
            assertThat(savedSpent.getPrice()).isEqualTo(spent.getPrice());
        }

    }

    @Nested
    class HasNotSpentWithId{

        @Test
        public void CheckIfExceptionIsThrow(){
            Long spentIdToFind = 1L;
            SpentNotFoundException ex = assertThrows(SpentNotFoundException.class,
                    () -> spentService.findById(spentIdToFind));
            assertThat(ex.getMessage())
                    .isEqualTo(String.format("Não existe gasto cadastrado com código %d", spentIdToFind));
        }

    }

}
