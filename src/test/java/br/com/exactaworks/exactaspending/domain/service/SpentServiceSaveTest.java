package br.com.exactaworks.exactaspending.domain.service;

import br.com.exactaworks.exactaspending.domain.model.Spent;
import br.com.exactaworks.exactaspending.domain.repository.SpentRepository;
import br.com.exactaworks.exactaspending.test.util.modebuilder.SpentModelBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class SpentServiceSaveTest {

    @Autowired
    private SpentRepository spentRepository;

    private SpentService spentService;

    private final SpentModelBuilder spentModelBuilder = new SpentModelBuilder();

    @BeforeEach
    public void setup(){
        spentService = new SpentService(spentRepository);
    }

    @Test
    public void checkSave(){
        Spent spent = spentModelBuilder.generate();
        assertDoesNotThrow(() -> {
            Spent savedSpent = spentService.save(spent);
            assertThat(savedSpent).isNotNull();
            assertThat(savedSpent.getId()).isNotNull();
            assertThat(savedSpent.getDateTime()).isEqualTo(spent.getDateTime());
            assertThat(savedSpent.getDescription()).isEqualTo(spent.getDescription());
            assertThat(savedSpent.getOwner()).isEqualTo(spent.getOwner());
            assertThat(savedSpent.getPrice()).isEqualTo(spent.getPrice());
        });
    }

    @AfterEach
    public void clearDB(){
        spentRepository.deleteAll();
    }

}
