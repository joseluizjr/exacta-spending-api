package br.com.exactaworks.exactaspending.api.controller;

import br.com.exactaworks.exactaspending.api.model.NewSpentModel;
import br.com.exactaworks.exactaspending.api.model.SpentDetailModel;
import br.com.exactaworks.exactaspending.domain.repository.SpentRepository;
import br.com.exactaworks.exactaspending.test.util.RequestUtil;
import br.com.exactaworks.exactaspending.test.util.modebuilder.NewSpentModelBuilder;
import org.junit.jupiter.api.*;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.StandardCharsets;


@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SpentControllerSaveValidTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private SpentRepository spentRepository;

    @Autowired
    private MessageSource messageSource;

    private MockMvc mockMvc;

    private final NewSpentModelBuilder newSpentModelBuilder = new NewSpentModelBuilder();

    private NewSpentModel spentToSave;

    private MvcResult response;

    private SpentDetailModel savedSpent;

    @BeforeAll
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        spentToSave = newSpentModelBuilder.generate();
        response = mockMvc.perform(post(RequestUtil.SPENT_BASE_URL)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(RequestUtil.serialize(spentToSave))).andReturn();
        savedSpent = (SpentDetailModel) RequestUtil.deserialize(response.getResponse()
                .getContentAsString(StandardCharsets.UTF_8), SpentDetailModel.class);
    }

    @AfterAll
    public void teardown(){
        spentRepository.deleteAll();
    }

    @Test
    public void checkStatusCodeCreated(){
        assertThat(response.getResponse().getStatus()).isEqualTo(HttpStatus.CREATED.value());
    }

    @Test
    public void checkBody(){
        assertThat(savedSpent.getId()).isNotNull();
        assertThat(savedSpent.getId()).isPositive();
        assertThat(savedSpent.getDateTime()).isEqualTo(spentToSave.getDateTime());
        assertThat(savedSpent.getDescription()).isEqualTo(spentToSave.getDescription());
        assertThat(savedSpent.getOwner()).isEqualTo(spentToSave.getOwner());
        assertThat(savedSpent.getPrice()).isEqualTo(spentToSave.getPrice());
        assertThat(savedSpent.getTags()).isEqualTo(spentToSave.getTags());
    }
}
