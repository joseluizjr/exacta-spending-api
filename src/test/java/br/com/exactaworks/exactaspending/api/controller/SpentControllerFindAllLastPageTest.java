package br.com.exactaworks.exactaspending.api.controller;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.data.domain.PageRequest;

import static org.assertj.core.api.Assertions.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SpentControllerFindAllLastPageTest extends AbstractSpentControllerFindAllTest{

    @BeforeAll
    public void setup() throws Exception {
        pageable = PageRequest.of(4, 20);
        super.setup();
    }

    @AfterAll
    public void teardown(){
        super.teardown();
    }

    @Test
    public void checkPageable(){
        super.checkPageable();
        assertThat(spentPage.isFirst()).isFalse();
        assertThat(spentPage.isLast()).isTrue();
        assertThat(spentPage.getPageable().getPageNumber()).isEqualTo(pageable.getPageNumber());
    }

}
