package br.com.exactaworks.exactaspending.api.controller;

import br.com.exactaworks.exactaspending.api.model.SpentDetailModel;
import br.com.exactaworks.exactaspending.domain.model.Spent;
import br.com.exactaworks.exactaspending.domain.repository.SpentRepository;
import br.com.exactaworks.exactaspending.test.util.RequestUtil;
import br.com.exactaworks.exactaspending.test.util.modebuilder.SpentModelBuilder;
import org.junit.jupiter.api.*;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.StandardCharsets;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SpentControllerFindByIdTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SpentRepository spentRepository;

    private MockMvc mockMvc;

    private MvcResult response;

    @BeforeAll
    public void setup(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class SpentFound{

        private final SpentModelBuilder spentModelBuilder = new SpentModelBuilder();

        private Spent spent;

        private SpentDetailModel spentDetailModel;

        @BeforeAll
        public void setup() throws Exception {
            spent = spentModelBuilder.generate();
            spent = spentRepository.save(spent);
            response = mockMvc.perform(get(RequestUtil.SPENT_BASE_URL + spent.getId())
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)).andReturn();
            spentDetailModel = (SpentDetailModel) RequestUtil.deserialize(response.getResponse()
                    .getContentAsString(StandardCharsets.UTF_8), SpentDetailModel.class);
        }

        @AfterAll
        public void teardown(){
            spentRepository.deleteAll();
        }

        @Test
        public void checkStatusCodeOk(){
            assertThat(response.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        }

        @Test
        public void checkSpentFound(){
            assertThat(spentDetailModel).isNotNull();
            assertThat(spentDetailModel.getId()).isEqualTo(spent.getId());
            assertThat(spentDetailModel.getPrice()).isEqualTo(spent.getPrice());
            assertThat(spentDetailModel.getDescription()).isEqualTo(spent.getDescription());
            assertThat(spentDetailModel.getOwner()).isEqualTo(spent.getOwner());
            assertThat(spentDetailModel.getDateTime()).isEqualTo(spent.getDateTime());
        }

    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class SpentNotFound{

        private String body;

        private final long radomInvalidId = new Random().nextLong() * (1L - Long.MAX_VALUE);

        @BeforeAll
        public void setup() throws Exception {
            response = mockMvc.perform(get(RequestUtil.SPENT_BASE_URL +  radomInvalidId)
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)).andReturn();
            body = response.getResponse().getContentAsString(StandardCharsets.UTF_8);
        }

        @Test
        public void checkStatusCodeOk(){
            assertThat(response.getResponse().getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        }

        @Test
        public void checkErrorMessage(){
            assertThat(body).isNotNull();
            assertThat(body).isNotEmpty();
            assertThat(body).contains(String.format("Não existe gasto cadastrado com código %d", radomInvalidId));
        }

    }

}
