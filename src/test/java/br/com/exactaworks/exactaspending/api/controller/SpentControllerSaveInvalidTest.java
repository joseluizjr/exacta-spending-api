package br.com.exactaworks.exactaspending.api.controller;

import br.com.exactaworks.exactaspending.api.model.NewSpentModel;
import br.com.exactaworks.exactaspending.domain.repository.SpentRepository;
import br.com.exactaworks.exactaspending.test.util.RequestUtil;
import br.com.exactaworks.exactaspending.test.util.modebuilder.NewSpentModelBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SpentControllerSaveInvalidTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private SpentRepository spentRepository;

    @Autowired
    private MessageSource messageSource;

    private MockMvc mockMvc;

    private final NewSpentModelBuilder newSpentModelBuilder = new NewSpentModelBuilder();

    private NewSpentModel spentToSave;

    private MvcResult response;

    @BeforeAll
    public void setup(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @BeforeEach
    public void generateRandoSpent(){
        spentToSave = newSpentModelBuilder.generate();
    }

    @ParameterizedTest(name = "Check owner constraints")
    @CsvSource({"1, a, O tamanho do campo Nome do usuário da despesa deve dev estar entre 2 e 150",
            "151, a, O tamanho do campo Nome do usuário da despesa deve dev estar entre 2 e 150",
            "0, , O campo Nome do usuário da despesa é obrigatório",
            "40, '', O campo Nome do usuário da despesa é obrigatório"})
    public void invalidOwner(int size, String letter, String message) throws Exception {
        spentToSave.setOwner(String.valueOf(letter).repeat(Math.max(0, size)));
        String json = tryGenerateNewSpent();
        checkResponse(json, message);
    }

    @ParameterizedTest(name = "Check description constraints")
    @CsvSource({"1, a, O tamanho do campo Descrição da despesa deve dev estar entre 2 e 150",
            "151, a, O tamanho do campo Descrição da despesa deve dev estar entre 2 e 150",
            "0, , O campo Descrição da despesa é obrigatório",
            "40, '', O campo Descrição da despesa é obrigatório"})
    public void invalidDescription(int size, String letter, String message) throws Exception {
        spentToSave.setDescription(String.valueOf(letter).repeat(Math.max(0, size)));
        String json = tryGenerateNewSpent();
        checkResponse(json, message);
    }

    @Test
    public void invalidDateTime() throws Exception {
        String message = "O campo Data e hora da despesa é obrigatório";
        spentToSave.setDateTime(null);
        String json = tryGenerateNewSpent();
        checkResponse(json, message);
    }

    @ParameterizedTest(name = "Check price constraints")
    @CsvSource({"0.00, O campo Valor da despesa deve ter um valor acima de 0.00",
            "10000000000000.00, O campo Valor da despesa deve ter um valor de até 9999999999999.99",
            "25.345, O campo Valor da despesa deve ter no máximo 13 dígitos antes da vírgula e 2 dígitos depos da vírgula",
            ", O campo Valor da despesa é obrigatório"})
    public void invalidPrice(String value, String message) throws Exception {
        spentToSave.setPrice(value == null ? null :new BigDecimal(value));
        String json = tryGenerateNewSpent();
        checkResponse(json, message);
    }

    @ParameterizedTest(name = "Check price constraints")
    @CsvSource({"0, , O campo Tags da despesa é obrigatório",
                "40, '', O campo Tags da despesa é obrigatório"})
    public void invalidTags(int size, String letter, String message) throws Exception{
        spentToSave.setTags(String.valueOf(letter).repeat(Math.max(0, size)));
        String json = tryGenerateNewSpent();
        checkResponse(json, message);
    }

    private String tryGenerateNewSpent() throws Exception {
        response = mockMvc.perform(post(RequestUtil.SPENT_BASE_URL)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(RequestUtil.serialize(spentToSave)))
                .andReturn();
        return response.getResponse().getContentAsString(StandardCharsets.UTF_8);
    }

    private void checkResponse(String body, String expectedMessage){
        assertThat(response.getResponse().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(body).isNotNull();
        assertThat(body).isNotEmpty();
        assertThat(body).contains(expectedMessage);
    }

}
