package br.com.exactaworks.exactaspending.api.controller;

import br.com.exactaworks.exactaspending.api.assembler.SpentListModelAssembler;
import br.com.exactaworks.exactaspending.api.model.SpentListModel;
import br.com.exactaworks.exactaspending.domain.model.Spent;
import br.com.exactaworks.exactaspending.domain.repository.SpentRepository;
import br.com.exactaworks.exactaspending.test.util.RequestUtil;
import br.com.exactaworks.exactaspending.test.util.RestResponsePage;
import br.com.exactaworks.exactaspending.test.util.modebuilder.SpentModelBuilder;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
public abstract class AbstractSpentControllerFindAllTest {

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Autowired
    protected SpentRepository spentRepository;

    protected final int spentListSize = 100;

    protected MockMvc mockMvc;

    protected final SpentModelBuilder spentModelBuilder = new SpentModelBuilder();

    protected MvcResult response;

    protected List<Spent> spents;

    protected Page<SpentListModel> spentPage;

    protected Pageable pageable;

    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        spents = spentModelBuilder.generateMany(spentListSize);
        spentRepository.saveAll(spents);
        response = mockMvc.perform(get(RequestUtil.SPENT_BASE_URL)
                .param("page", String.valueOf(pageable.getPageNumber()))
                .param("size", String.valueOf(pageable.getPageSize()))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        spentPage = (RestResponsePage<SpentListModel>) RequestUtil.deserialize(response.getResponse()
                .getContentAsString(StandardCharsets.UTF_8), RestResponsePage.class);
    }

    public void teardown(){
        spentRepository.deleteAll();
    }

    @Test
    public void checkStatusCode(){
        assertThat(response.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    public void checkPageable(){
        assertThat(spentPage).isNotNull();
        assertThat(spentPage.getTotalElements()).isEqualTo(spentListSize);
        assertThat(spentPage.getContent()).isNotNull();
        assertThat(spentPage.getContent()).isNotEmpty();
        assertThat(spentPage.getTotalPages()).isEqualTo(spentListSize / pageable.getPageSize());
        assertThat(spentPage.getNumberOfElements()).isEqualTo(pageable.getPageSize());
    }

}
