package br.com.exactaworks.exactaspending.test.util.modebuilder;

import com.github.javafaker.Faker;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractModelBuilder<T> {

    @Getter
    protected Faker faker = new Faker();

    abstract T generate();

    public List<T> generateMany(int size){
        List<T> models = new ArrayList<>();
        for(int i = 0; i < size; i ++){
            models.add(generate());
        }
        return models;
    }

}
