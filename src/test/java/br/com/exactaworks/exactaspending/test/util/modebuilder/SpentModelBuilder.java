package br.com.exactaworks.exactaspending.test.util.modebuilder;

import br.com.exactaworks.exactaspending.domain.model.Spent;
import com.github.javafaker.Faker;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneId;

public class SpentModelBuilder extends AbstractModelBuilder<Spent>{

    public Spent generate(){
        Spent spent = new Spent();
        spent.setOwner(faker.name().firstName());
        spent.setDescription(faker.lorem().word());
        spent.setPrice(BigDecimal.valueOf(faker.number().randomDouble(2, 0, 9999)));
        spent.setDateTime(OffsetDateTime.ofInstant(faker.date().birthday().toInstant(), ZoneId.systemDefault()));
        spent.setTags(StringUtils.join(faker.lorem().words(), ","));
        return spent;
    }
}
