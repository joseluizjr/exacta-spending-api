package br.com.exactaworks.exactaspending.test.util.modebuilder;

import br.com.exactaworks.exactaspending.api.model.NewSpentModel;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneId;

public class NewSpentModelBuilder extends AbstractModelBuilder<NewSpentModel>{

    @Override
    public NewSpentModel generate() {
        NewSpentModel newSpentModel = new NewSpentModel();
        newSpentModel.setOwner(faker.name().firstName());
        newSpentModel.setDescription(faker.lorem().word());
        newSpentModel.setPrice(BigDecimal.valueOf(faker.number().randomDouble(2, 0, 9999)));
        newSpentModel.setDateTime(OffsetDateTime.ofInstant(faker.date().birthday().toInstant(), ZoneId.systemDefault()));
        newSpentModel.setTags(StringUtils.join(faker.lorem().words(), ","));
        return newSpentModel;
    }
}
