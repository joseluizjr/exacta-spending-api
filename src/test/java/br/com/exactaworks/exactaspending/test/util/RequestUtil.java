package br.com.exactaworks.exactaspending.test.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class RequestUtil {


    public final static String SPENT_BASE_URL = "/spents/";

    private static ObjectMapper getObjectMapper(){
        return new ObjectMapper().registerModule(new JavaTimeModule());
    }

    public static String serialize(Object object) throws JsonProcessingException {
        return getObjectMapper().writeValueAsString(object);
    }

    public static Object deserialize(String json, Class<?> typeReturn) throws JsonProcessingException {
        return getObjectMapper().readValue(json, typeReturn);
    }

}
